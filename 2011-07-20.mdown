#System and Device Programming#
##20 July 2011 – Theory Solutions##

###1
Show and describe the fields of a Unix Inode. It is possible that a 1 Mbytes file occupies less than 256 blocks of 4 Kbytes? If yes, how the file has been created, and what does each block contain?

*(answer by M. Ardizzone)*

The inode unix structure is made of different fields:

+ Owner and group
+ Type of the file: it can be a regular file, a directory or a special file
+ Access rights and access time
+ File size
+ Link count: indicates how many hard links point to that inode
+ Pointer to the disk block storing the actual data

*(Not sure if the question “it's possible to occupy less than 256 blocks of 4KB each?” makes any sense).*

*My suggestion is to bring everything to bytes (basic unit of measure) and then do the math, which is pretty straightforward (and probably stupid):*

(1\*1024\*1024)/(4*1024) = 256 → Blocks occupied by 1MB file

Tried on my PC with a file of 1154863 bytes, my filesystem uses blocks of size 512 bytes each, so:

1154863 / 512 = 2256 blocks occupied

That's the same value that `ls -s` returns if run on that file.

*(answer by G. David)*

The fields of a Unix inode are:

  + Owner and group
  + Number of links: how many hard links point to the file
  + Access rights: the rights (read, write, execute) of the file
  + Access times
  + File size
  + Type of file: directory, normal, special, ...
  + Table of pointers to data blocks

Yes it is possible if the file has been created with a command similar to the following:

    dd if=/dev/zero of=file bs=4K count=1 seek=255

In this case the file is 1Mb, since we wrote one block in output after skipping 255 blocks of size 4K.
However only this last block contains data, all other blocks are null.


###2
Complete the scheme of two parallel cyclic threads, inserting the appropriate semaphore calls and variable management that allow printing either the word ABCD or BACD.

*(answer by M. Ardizzone)*

**Process 1:**

    init(S1,0);
    init(S3,0);
    S(S1);
    while(TRUE){
      W(S1);
      printf(“A”);
      W(S2);
      printf(“C\n”);
      S(S3)
      W(S4);
      S(S1)
    }

**Process 2:**

    init(S1,0);
    init(S2,0);
    init(S4,0);
    S(S1)
    while(TRUE){
      W(S1);
      printf(“B”);
      S(S2)
      W(S3);
      printf(“D\n”);
      S(S4);
      S(S1);
    }

###3
Write the sequence of Unix system calls that allow a process to change its code, using a system call of the exec family, to execute the command ”ls -l” redirecting its output to a temporary file (not seen by the other processes) output.txt.

*(answer by M. Ardizzone)*

    #include <stdio.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <fcntl.h>

    int main (int argc, char ** argv)
    {
      int fd = open("output.txt",O_WRONLY|O_CREAT|O_TRUNC);
      dup2(fd,fileno(stdout));
      if (execlp ("ls","Listing","-l", NULL) < 0)
        printf ("error exec\n");
      return 0;
    }

###4
Explain the main features of memory mapped files in Win32. In particular, list the main functions (or operations) that can be used to work on memory mapped files and their differences with respect to sequential and direct file access routines.

*(answer by M. Ardizzone)*

Memory mapping in win32 in done essentialy in two steps:

1. We first create a mapping of the file in memory through `CreateFileMapping(..)`:
    
    This function requires the handle to the file on which we want to create the mapping and the maximum size of the mapping in memory (plus other parameters like the protection and name of the mapping).
    The function will return a HANDLE to the memory map
2. We make a view of the memory mapping through `MapViewOfFile(..)` that requires as main parameters the handle to the memory map, the starting point of the view and the size of the view.

The advantage of having memory mapping split into two "steps" is that we can create a mapping of a file once, but perform **multiple** views on the same file, which allows us to fetch different data from different portions of the file.

Then since it's a mapping of a file in memory, we can work with pointers and every operation is done in memory, so there is no need for continuous I/O through read and write function calls.

###5
Describe the main features of standard IO devices and console in Win32. Is it possible to write a function `PrintMsg` that, after receiving as parameters a handle to a file and a message, exploits different output functions for standard files and for the console? If it is possible, write an example in C language.

*(answer by M. Ardizzone)*

In Win32 there are the three common IO devices (stdin,stdout and stderr) and console IO devices.

Every device is identified by a HANDLE (that can be obtained through `GetStdHandle()`) instead of a file desriptor as in UNIX systems.

IO devices as stdin/stdout/stderr are open files by default on many OS and they can be redirected and shared (among processes and threads).

Console devices have specific interfaces related to screen and keyboard. Unlike stdin/stdout/stderr which can be accessed through common calls for file handling, consoles have specific routines.

Here is an example of `PrintMsg` function:

    VOID PrintMsg (HANDLE hOut, TCHAR *msg){    
       DWORD MsgLen, Count;
        MsgLen = _tcslen (msg);
        /*
          WriteConsole fails if the handle is associated with a file rather than with
          a console.
        */
        if (!WriteConsole(hOut, msg, MsgLen, &Count, NULL) && !WriteFile(hOut, msg, MsgLen * sizeof(TCHAR), &Count, NULL))
          return;
    }

