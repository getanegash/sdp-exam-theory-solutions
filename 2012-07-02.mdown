#System and Device Programming#
##2 July 2012 – Theory Solutions##

###1
Explain the difference between the **canonical**, **raw** and **cbreak** modes of the terminal line disciplines.

*(answer by M. Ardizzone)*

+ Canonical: The line discipline performs all the required editing BEFORE passing the line to the process (processes read only entire lines that were previously edited). Character sequences related to “interrupt” or “quit” are translated to the corresponding signals before passing them to the process
+ Raw: As the name suggests, no line editing is performed. So sequences for “interrupt” or “quit” are passed as normal character input. Characters are received by the process as they are input.
+ Cbreak: Similar to raw mode, where no line editing is performed and control sequences are treaded as normal character input. Only “interrupt” and “quit” character sequences and modem flow control characters are stripped from the input stream and properly handled.

###2
What is the effect of these bash commands?

    dd if=/dev/zero of=hd.img bs=512 count=1 seek=$((256*1024))
    losetup /dev/loop1 hd.img

*(answer by M. Ardizzone)*

The first instruction (dd) fills the output file (hd.img) with zeroes coming from /dev/zero (this is a special devices that outputs 0). It's a way to initialize a device.

Moreover, it starts from the 262144-th byte (seek parameter) from the beginning of the file output file and copies 1 block of 512 bytes.

The second instruction is used to associate the loop device /dev/loop1 to hd.img

*(answer by G. David)*

    dd if=/dev/zero of=hd.img bs=512 count=1 seek=$((256*1024))

This command copies a single block of size 512B from /dev/zero (so it is a block filled with 0) to the file hd.img.
This block is copied to the output size after skipping 256\*1024 blocks, so the output will have size 512\*(1+256\*1024) bytes, but it will contain mostly null bytes.

    losetup /dev/loop1 hd.img

This command associates the file hd.img to the loop device /dev/loop1. This is used to make the file appear as a block device. Any operation done on /dev/loop1 is mapped to the file.
To delete the association the command “losetup -d /dev/loop1” is used


###3
Implement this precedence graph with the minimum number of semaphores. The processes are 5, and they are cyclic. Notice that the small dots in the graph are synchronization points, which cannot be substituted by a dummy process.

*(answer by M. Ardizzone)*

**Main:**

    init(S1,2);
    init(S2,2);
    init(S3,2);
    init(S4,-2);
    init(S5,-2);

**Process 1:**

    while(1){
      W(S1);
      W(S1);
      S(S4);
      S(S5);
    }

**Process 2:**

    while(1){
      W(S2);
      W(S2);
      S(S4);
      S(S5);
    }

**Process 3:**

    while(1){
      W(S3);
      W(S3);
      S(S4);
      S(S5);
    }

**Process 4:**

    while(1){
      W(S4);
      W(S4);
      W(S4);
      S(S1);
      S(S2);
      S(S3);
    }

**Process 5:**

    while(1){
      W(S5);
      W(S5);
      W(S5);
      S(S1);
      S(S2);
      S(S3);
    }


*(answer by G. David)*

**Main:**

    init(s1, 2)
    init(s2, 2)
    init(s3, 2)
    init(s4, 0)
    init(s5, 0)

**T1**

    while (TRUE) {
    	wait(s1)
    	wait(s1)
    	T1
    	signal(s4)
    	signal(s5)
    }

**T2**

    while (TRUE) {
    	wait(s2)
    	wait(s2)
    	T2
    	signal(s4)
    	signal(s5)
    }

**T3**

    while (TRUE) {
    	wait(s3)
    	wait(s3)
    	T3
    	signal(s4)
    	signal(s5)
    }

**T4**

    while (TRUE) {
    	wait(s4)
    	wait(s4)
    	wait(s4)
    	T4
    	signal(s1)
    	signal(s2)
    	signal(s3)
    }

**T5**

    while (TRUE) {
    	wait(s5)
    	wait(s5)
    	wait(s5)
    	T5
    	signal(s1)
    	signal(s2)
    	signal(s3)
    }

###4
Discuss characteristics, advantages and disadvantages of synchronization object in the Windows systems with particular attention to critical sections, mutexes, semaphores, and events.

*(answer by M. Ardizzone)*

**Critical Section** is the only synchronization object among the four that is not a kernel object, thus, it cannot be used for synchronizing different processes (or threads belonging to different processes). It can only be used to synchronize threads belonging to the same process.

A critical section is initialized through `InitializeCriticalSection(LPCRITICAL_SECTION lpCriticalSection)` function call.
It can then be used for executing some instructions in mutual exclusion by calling `EnterCriticalSection(LPCRITICAL_SECTION lpCriticalSection)` and then when we are done on the mutual exclusion part, we exit the critical section with `LeaveCriticalSection(LPCRITICAL_SECTION lpCriticalSection)`.
Finally, its resources are released with `DeleteCriticalSection(LPCRITICAL_SECTION lpCriticalSection)`

It provides the same functionality as mutex object but is slightly faster.

**Mutex** is a synchronization object that can be used to execute some parts of the code in mutual exclusion.

It is created by calling `CreateMutex(...)`: as parameters we can specify if the mutex is created as already taken (so other processes/threads must wait for its release) and we can give it a name: this is because Mutex is kernel object and as such it can be used by other processes (through `OpenMutex(...)` call). After the creation of the mutex a HANDLE is returned.

To get the ownership of the mutex, we use `WaitForSingleObject(...)`, while to release it we call `ReleaseMutex(...)`. Finally, its resources are release by simple using `CloseHandle(...)` on the mutex handle.

Mutex works similarly to a semaphore but unlike a sempahore, it has no counter.

**Semaphore** as mutex is a kernel object, can be named and can be used by other processes (`OpenSemaphore(...)`). It's created with `CreateSemaphore(...)` where we can specify the initial counter of the semaphore and its maximum allowed value.

A call to `WaitForSingleObject(...)` decrements the counter (if it is greater than 0) and allows the thread to go past the wait, otherwise the thread will wait.

A call to `ReleaseSemaphore()` increases the counter of the semaphore by the number specified as parameter.
Its resources are released with `CloseHandle(...)`.

An **Event** is also a kernel object and is created with `CreateEvent(...)`: here we can specify if the event is created in a signalled/non-signalled state and if the state is reset automatically after it is caught.

The signal can be caught with `WaitForSingleObject(...)` and the event remains signalled if it was created with a manual-reset, so a call to `ResetEvent(...)` might be needed.

An event can be signalled by calling `SetEvent(...)` or `PulseEvent(...)` and in combination with the manual/auto reset it behaves differently:

|   |Auto Reset|Manual Reset |
|---|----------|-------------|
|`SetEvent()`|Exactly one thread is released. If none are currently waiting on the event, the next thread to wait will be released.|All currently waiting threads are released. The event remains signalled until reset by some thread.|
|`PulseEvent()`|Exactly one thread is released, but only if a thread is currently waiting on the event.|All currently waiting threads are released, and the event is then reset.|

Resources of an event are released through `CloseHandle(...)`.

###5
Discuss how to perform exception handling in the Windows systems.

*(answer by M. Ardizzone)*

Exception handling can be done through the constructs `__try` and `__except/__finally`.

Here is an example of try/except:

    __try{
      //Do something that might crash the program
    }__except(FilterCondition(GetExceptionCode())){
      //An exception was caught and here we gracefully terminate the program
    }

When an exception is raised in the try block, the `FilterCondition` is executed: we can pass it the exception code by calling `GetExceptionCode()`. Inside the filter condition we can perform some checks on the exception code and react differently. In any case, FilterCondtion must return one of the following:

  + `EXCEPTION_EXECUTE_HANDLER`: the except block is executed.
  + `EXCEPTION_CONTINUE_SEARCH`: this handler is not in charge of processing this exception, so unwind the stack and search for the next exception handler.
  + `EXCEPTION_CONTINUE_EXECUTION`: ignores the exception.

Here is an example of try/finally:

    __try{
      //Do something that might crash the program
    }__finally{
      //This is executed in any case
    }

Finally, we can combine try, except and finally by nesting the finally inside a try block as follows:

    __try {
      __try {
        //1
      }__finally{
        //3
      }
    }__except(FilterCondition(GetExceptionCode())){ //2
      //4
    }

In case an exception in 1 occurs, then the filter condition is executed (2), then the finally (3) and in the end the except (4) terminates the program.
